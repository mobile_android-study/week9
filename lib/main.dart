import 'package:flutter/material.dart';
import 'dart:async';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'dog.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final Future<Database> database = openDatabase(
    join(await getDatabasesPath(), 'doggie_database.db'),
    onCreate: (db, version) {
      return db.execute(
          'CREATE TABLE dogs(id INTEGER PRIMARY KEY, name TEXT, age INTEGER)');
    },
    version: 1,
  );
  Future<void> insertDog(Dog dog) async {
    final db = await database;
    await db.insert('dogs', dog.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  var fido = Dog(id: 0, name: 'Fido', age: 7);
  var dido = Dog(id: 1, name: 'Dido', age: 5);
  await insertDog(fido);
  await insertDog(dido);

  Future<List<Dog>> dogs() async {
    final db = await database;
    return Dog.toList(await db.query(
      'dogs',
    ));
  }

  print(await dogs());

  Future<void> updateDog(Dog dog) async {
    final db = await database;

    await db.update(
      'dogs',
      dog.toMap(),
      where: 'id = ?',
      whereArgs: [dog.id],
    );
  }

  fido = Dog(
    id: fido.id,
    name: fido.name,
    age: fido.age + 7,
  );

  Future<void> deleteDog(int id) async {
    final db = await database;
    await db.delete(
      'dogs',
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  await updateDog(fido);
  print(await dogs());
}
